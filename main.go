package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"
	"strconv"
	"sync"
	"time"
)

func main() {
	cnt := flag.Int("votes", 50, "Amount of votes per proposal")
	workers := flag.Int("workers", 5, "Amount of workers to use")
	timeout := flag.Int("timeout", 15, "Request timeout in seconds.")
	all := flag.Bool("all", false, "Vote for all proposals")
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage:\n  %s [options] proposal-id\n\nOptions:\n", os.Args[0])
		flag.PrintDefaults()
	}
	flag.Parse()

	if len(flag.Args()) == 0 && !*all {
		flag.Usage()
		os.Exit(1)
	}
	_ = runtime.GOMAXPROCS(runtime.NumCPU())
	var wg sync.WaitGroup
	var props []int
	// Change these if you feel like it
	DefaultHeaders := map[string]string{
		"Cookie":           `ProposalVoteSupport=; ProposalVote_1=voted`,
		"Origin":           `https://service.lu.se`,
		"Accept-Encoding":  `deflate, br`,
		"Accept-Language":  `en-GB,en;q=0,8`,
		"User-Agent":       `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.40 Safari/537.36`,
		"Content-Type":     `application/json`,
		"Accept":           `*/*`,
		"Referer":          `Referer: https://service.lund.se/Citizen/Proposal`,
		"X-Requested-With": `XMLHttpRequest`,
		"Connection":       `keep-alive`,
	}

	if *all {
		p, err := GetAllProposals(DefaultHeaders, "https://service.lund.se/Citizen/Proposal/GetProposals", &ListRequest{Params: Parameters{PageSize: 100, SortField: "DateTo"}})
		if err != nil {
			log.Fatal(err)
		}
		props = p
	} else {

		for _, p := range flag.Args() {
			pr, err := strconv.Atoi(p)
			if err != nil {
				flag.Usage()
				os.Exit(1)
			}
			props = append(props, pr)
		}
	}
	jobs := make(chan *Job)
	for i := 0; i < *workers; i++ {
		go worker(jobs, &wg)
	}

	for i := 0; i < *cnt; i++ {
		for _, id := range props {
			jobs <- &Job{Data: &Payload{ID: id}, Destination: "https://service.lund.se/Citizen/Proposal/SendVote", Headers: DefaultHeaders, Timeout: time.Second * time.Duration(*timeout)}
		}
	}

	close(jobs) // this kills the worker
	wg.Wait()
}
