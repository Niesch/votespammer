package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"
)

// GetAllProposals returns a slice of all IDs and error, if any.
func GetAllProposals(hdrs map[string]string, dst string, lr *ListRequest) ([]int, error) {
	client := &http.Client{
		Timeout: 10 * time.Second,
	}
	buf := new(bytes.Buffer)
	json.NewEncoder(buf).Encode(lr)
	req, err := http.NewRequest("POST", dst, buf)
	if err != nil {
		return []int{}, err
	}
	for k, v := range hdrs {
		req.Header.Add(k, v)
	}
	resp, err := client.Do(req)
	if err != nil {
		return []int{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return []int{}, err
	}
	var lrp ListResponse
	var ids []int
	err = json.Unmarshal(body, &lrp)
	if err != nil {
		return []int{}, err
	}
	for _, r := range lrp.Result {
		ids = append(ids, r.ID)
	}
	return ids, nil
}
