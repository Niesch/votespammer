package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sync"
)

// worker accepts jobs over channel jobs until it is closed
func worker(jobs <-chan *Job, wg *sync.WaitGroup) {
	wg.Add(1)
	defer wg.Done()
	for j := range jobs {
		err := vote(j)
		if err != nil {
			log.Println(err)
		} else {
			fmt.Printf(".")
		}
	}
}

// vote takes a single job and performs it
// returns error if any, nil elsewise
func vote(j *Job) error {
	client := &http.Client{
		Timeout: j.Timeout,
	}
	buf := new(bytes.Buffer)
	json.NewEncoder(buf).Encode(j.Data)
	req, err := http.NewRequest("POST", j.Destination, buf)
	if err != nil {
		return err
	}
	for k, v := range j.Headers {
		req.Header.Add(k, v)
	}
	_, err = client.Do(req)
	if err != nil {
		return err
	}
	return nil
}
