package main

import (
	"time"
)

type Payload struct {
	ID int `json:"id"`
}

type Job struct {
	Data        *Payload
	Destination string
	Headers     map[string]string
	Timeout     time.Duration
}

// The following structs represent the relation in the API required for
// obtaining the IDs for all proposals.
type ListRequest struct {
	Params Parameters `json:"parameters"`
}

type Parameters struct {
	Filter               string `json:"Filter"`
	PageSize             int    `json:"PageSize"`
	SortField            string `json:"SortField"`
	AdditionalParameters struct {
		FilterText      string `json:"FilterText"`
		IncludeFinished bool   `json:"IncludeFinished"`
	} `json:"AdditionalParameters"`
}

type ListResponse struct {
	TotalRows int `json:"TotalRows"`
	Result    []struct {
		ID           int    `json:"Id"`
		Title        string `json:"Title"`
		VoteCount    int    `json:"VoteCount"`
		CommentCount int    `json:"CommentCount"`
		StateString  string `json:"StateString"`
		DateTo       string `json:"DateTo"`
	} `json:"result"`
}
