# votespammer
This utility spams votes for lund.se "Lundaförslaget"

## Why?
Currently, there's no captcha, rate limiting or IP checking performed on people who vote. Considering this system is meant to be something democratic, it's rather silly to have it be that way. By enabling people to spam votes on anything they like, we can hopefully make a change here.

## How?
Pull the code, build using `go build`.

Run it without arguments or with invalid arguments to see usage flags.

The -all flag will spam vote every vote. For select votes, just provide their IDs as individual arguments.

Use responsibly.
